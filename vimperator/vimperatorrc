" Vimperator Configuration
" ------------------------
"
" :ju[mps] - List all jumps in current tab's history.
" :w[rite] - Save's current webpage to current directory.
" :pa[geinfo] - View information about current page.
" :se[t]  {pref}={value} - Set any 'vimperator' option.
" :se[t]! {pref}={value} - Set any 'about:config' option.
" :buffers [filter] - List buffers filtered (also, just use 'B' key)
" :tabm[ove] N - Move tab to the N'th place.
"
" Keybinds:
"      a - Bookmark current page.
"     ;; - Focus target element.
"     ;s - Save target link's destination.
"     ;S - Save target media element.
"     ;y - Copy target hint's contents to clipboard.
"     ;i - Open target media element in current tab.
"     ;I - Open target media element in new tab.
"     gu / gU - Go to parent directory / root directory.
"     g^ / g$ - Go to First / Last Tab.
" Quick Jumps {{{1

" Basic Settings {{{1
set shell=bash
set shell=vim

" Set hint characters to be 'smart hints' which uses more keys but favors
" the home row keys.
set hintchars=hjklasdfgyuiopqwertnmzxcvb

" Disable scrollbars, since they are really annoying.
set noscrollbars

" Enables some pretty nice animations for vimperator.
set animations

" Complete based on local files, buffers, search engines, (but not history)
set complete=bflsSt

" List all auto-complete results and complete the first one.
set wildmode=list:full

" Set default search engine to DDG."
" set defsearch=duckduckgo

" Try stay in normal mode instead of auto-selecting an input field.
set focuscontent

" Follow a hint as soon as its unique enough to identify it.
set followhints=0

" For case-insensitive searching.
set smartcase

" Maximum number of auto-completions.
set maxitems=8

" Expiration on statusline messages (ms).
set messagetimeout=3000

" Open all firefox windows in a new tab instead of a new window.
set newtab=all

" Change window title string
set titlestring="Firefox"

" Configure which gui elements are enabled for by default.
set gui=noaddons,nobookmarks,nomenu,navigation,tabs

" Sanitize everything from today things when `:sanitize` is used.
" Items to sanitize when `:sanitize` is used.
set si=cache,commandline,cookies,downloads,formdata,history,marks,sessions

" How far back sanitize should clean when `:sanitize` is used.
" Options: 0=everything, 1=1hr, 2=2hrs, 3=4hrs, 4=today.
set sts=4

" Show link destinations in the status line of vimperator.
set showstatuslinks=2

" Toggle switches for vimperator.
nnoremap ]a :set apptab<cr>
nnoremap [a :set noapptab<cr>

nnoremap ]A :set animations<cr>
nnoremap [A :set noanimations<cr>

nnoremap ]g :set gui=noaddons,nobookmarks,nomenu,navigation,tabs<cr>
nnoremap [g :set gui=none<cr>


" Set default 'pageinfo' to be the more verbose version.
nnoremap <C-g> :pageinfo<cr>
nnoremap yy    :bitly<cr>

" Colors {{{1
highlight Bell border: 0 none; background-color: #222222;
highlight CmdLine font-size:8px;
highlight CompDesc color: #c6c6c6; width: 50%; font-size: 10px;
highlight CompItem[selected] background: #343434; color: #afafaf;
highlight CompResult width: 45%; overflow: hidden;font-size: 10px; font-family: monospace; font-weight: normal;
highlight CompTitle color: #afafaf; background: #222222; font-weight: bold;
highlight CompTitle>* padding: 0 .5ex;
highlight ErrorMsg color: #ffffff; background: #973029; font-weight: bold;
highlight Filter font-weight: bold;color: #afafaf;
highlight FrameIndicator background-color: #222222; opacity: 0.5; z-index: 999; position: fixed; top: 0; bottom: 0; left: 0; right: 0;
highlight Function color: lightgreen; background: #222222;
highlight Hint font-family: Sans; min-width:16px; min-height:16px; text-align:center; font-size: 12px; font-weight: bold; color: #FEFEFE; background-color: #39F; border-color: white; border-width: 2px; border-radius: 4px; border-style: solid; padding: 0.25em;
highlight HintActive background: #F93; color: #f6e9e9;
highlight HintElem background-color: #222222; color: #f6e9e9;
highlight InfoMsg color: #afafaf; background: #222222;
highlight LineNr color: #afafaf; background: #222222;
highlight ModeMsg color: #9f9f9f; background: #111111; "CmdLine when Not in Use
highlight MoreMsg color: #9f9f9f; background: #111111;
highlight NonText color: lightblue; min-height: 16px; padding-left: 2px;
highlight Normal color: #afafaf; background: #222222;
highlight Number color: tan; background: #222222
highlight Question color: #ffffff; background: #222222; font-weight: bold;
highlight StatusLine color: #afafaf; background: #222222; font-size: 8px;
highlight String color: #222222; background: #afafaf;
highlight Tag color: #222222; background: #afafaf;
highlight Title color: #222222; background: #afafaf; font-weight: bold;
highlight URL text-decoration: none; color: lightblue; background: inherit;
highlight WarningMsg color: red; background: #222222;

" Statusbar Custom Style {{{1
style! -name=statusbar chrome://* <<EOF
#status-bar statusbarpanel { padding: 0 1px !important; }
statusbarpanel > * { margin: 0 !important; padding: 0 2px 0 0 !important; }
EOF

" Make bar yellow when focused.
" From: http://www.reddit.com/r/linux/comments/99d55/i_could_use_a_little_vimperator_help_also/
javascript <<EOF
    (function() {
        var inputElement = document.getElementById('liberator-commandline-command');
        function swapBGColor(event) {
            inputElement.style.backgroundColor = event.type == "focus" ? "yellow" : "";
        }
        inputElement.addEventListener('focus', swapBGColor, false);
        inputElement.addEventListener('blur', swapBGColor, false);
    })();
EOF

" Use 'kk' like I use it in the shell. Reset & reload.
command! kk -description "Reload Vimperator configuration." source ~/.vimperatorrc

" Quick Open Commands.
command! docs      -description "Open Google Docs in a new tab."  tabopen https://g.co/docs
command! dyngh     -description "Dyn's Github in a new tab."      tabopen https://bit.ly/1CC4FJg
command! gcal      -description "Google Calendar in a new tab."   tabopen https://g.co/calendar
command! gh        -description "Github in a new tab."            tabopen https://github.com
command! hangouts  -description "Google Hangouts in a new tab."   tabopen https://g.co/hangouts
command! lpass     -description "LastPass in a new tab."          tabopen https://lastpass.com
command! mail      -description "Google Mail in a new tab."       tabopen https://gmail.com
command! pkt       -description "Pocket in a new tab."            tabopen https://getpocket.com
command! sheets    -description "Google Sheets in a new tab."     tabopen https://g.co/sheets
command! slack     -description "Slack in a new tab."             tabopen https://dyn.slack.com

" Always make slack a pinned tab.
autocmd LocationChange dyn\\.slack\\.com    :set apptab
autocmd LocationChange mail\\.google\\.com  :set apptab

" Use autosource plugin to automatically source vimperatorrc on change.
"autosource ~/.vimperatorrc

source! /home/jake/.vimperatorrc.local
" vim: ft=vim foldmethod=marker
