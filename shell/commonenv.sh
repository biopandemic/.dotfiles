# Common Shell Environment.
# -------------------------
# I use ZSH, but many things require bash. These are the functions that should
# be run in both environments.


# Set Console Environment
# -----------------------
# Sets a large font for my retina display.
# TODO: This should only happen on my laptop, for now I just check hostname.

# setfont Uni3-Terminus32x16


# Set Common ShellEnvironment Variables
# -------------------------------------
# Set default programs.
export BROWSER="firefox"
export EDITOR="vim"

cless(){ hcat "$1" | less -R ;}
export PAGER='vim -u /usr/share/vim/vim74/macros/less.vim'

# if [[ ! -z $DISPLAY ]]; then
#     export EDITOR="gvim -f"
#     export EDITOR_CLI="gvim"
#     alias vim=$EDITOR_CLI
#     alias vi=$EDITOR_CLI
#     alias v=$EDITOR_CLI
#     alias e=$EDITOR_CLI
#     alias editor=$EDITOR_CLI
#     alias edit=$EDITOR_CLI
# fi

# Path manipulation.
export PATH="/sbin:/bin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin"
export MANPATH="/usr/local/man:$MANPATH"

# Commonly used directory constants.
export BASH="$HOME/.bash"
export ZSH="$HOME/.zsh"

# Dotfile-specific constants.
export DOTFILES="$HOME/.dotfiles"
export DF_LOGS="$DOTFILES/logs"

# Misc
export ARCHFLAGS="-arch x86_64"
export LANG='en_US.UTF-8'


# Set Common X11 Environment Variables
# ------------------------------------
# Prefer keeping all multimedia in  ~/Media.
mkdir -p $HOME/Media 1>/dev/null 2>&1

export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DESKTOP_DIR="$HOME/Desktop"
export XDG_DOCUMENTS_DIR="$HOME/Media/Documents"
export XDG_DOWNLOAD_DIR="$HOME/Downloads"
export XDG_MUSIC_DIR="$HOME/Media/Music"
export XDG_PICTURES_DIR="$HOME/Media/Pictures"
export XDG_PUBLICSHARE_DIR="$HOME/Media/Public"
export XDG_TEMPLATES_DIR="$HOME/.Templates"
export XDG_VIDEOS_DIR="$HOME/Media/Videos"


# Define base aliases for shell env.
# ---------------------------------
_ls='ls -FhvC'
alias ls="${_ls}"
alias ll="${_ls} -l"
alias l="${_ls} -lA"

alias e="$EDITOR"
alias vi="vim"

# Force oure pager to be all the pagers.
alias less=$PAGER
alias zless=$PAGER

# Job Management Aliases
# alias f="fg '%${1}'"
# alias b="bg '%${1}'"
# alias j="jobs -l"
# alias k="kill '%${1}'"

alias mkdir='mkdir -pv'                   # Create parent dirs as needed
alias wget='wget -c'                        #wget - default to "resume"
alias ping='ping -c 5'
alias cp="cp -i"                          # confirm before overwriting something
alias df='df -H'                          # human-readable sizes
alias du="du -ch"
alias free='free -m'                      # show sizes in MB
alias grep='grep --color=tty -d skip'     # colorize!

# Netstat/Port Handling
alias lsports="sudo netstat -pant"
alias ports='netstat -tulanp'
alias ping.fast='ping -c 100 -s.2'

# Displays the number of times this word occurs in CWD.
keywordFreq(){ ack $@ -i . | wc -l ;}


# Define Common Functions
# --------------------------------
ssh-forget(){ sed -i "${1}d" ~/.ssh/known_hosts ;}

# Configuration Helper Functions
is_installed(){ test -x "$(which $@ 2>/dev/null)" 2>/dev/null ;}

# Add to path only if it's not already added and if the directory exists. Common
# sense, really.
# Usage: addToPath <BIN_DIR> [end] ('end' means append)
addToPATH(){
    [[ ":$PATH:" == *":$1:"* ]] && return
    [[ ! -d $1  ]] && return

    if [[ "${2:-beg}" == "end" ]]; then
        export PATH="${PATH}:$1"
    else
        export PATH="$1:${PATH}"
    fi

}

# Print path environment in a more sane order
printPATH(){ echo "Current PATH:"; echo ":$PATH:" | tr -s ':' '\n'; echo "" ;}

# Add Dotfiles to the beginning of my path.
addToPATH "$DOTFILES/bin"

try-source(){ test -e "$@" 2>/dev/null && source "$@" ;}


# Maintenance Functions (genpasswd, mountIso, etc)
# ------------------------------------------------
genpasswd() {
    local l=${1:-20}
    tr -dc A-Za-z0-9_ < /dev/urandom | head -c ${l} | xargs
}

# Handling ISO files (mount, unmount)
mountIso() { mkdir ~/ISO_CD && sudo mount -o loop "$@" ~/ISO_CD && cd ~/ISO_CD && ls; }
umountIso() { cd ~ && sudo umount ~/ISO_CD && rm -r ~/ISO_CD; }

# Make a directory and then `cd` to it.
mcd() { mkdir -p "$1" && cd "$1"; }


# Path configurations
# -------------------
addToPATH "$HOME/.bin"
addToPATH "$HOME/bin"
addToPATH "$HOME/.local/bin"

# Custom Vim-Specific Commands.
# -----------------------------
alias wiki="$EDITOR -c VimwikiIndex"
alias jrnl="$EDITOR -c VimwikiDiaryIndex"

qref(){cd $DOTFILES/qrefs;$EDITOR -m $(find . -type f -name '*.md' | fzf -1);}
eqref(){cd $DOTFILES/qrefs;$EDITOR $(find . -type f -name '*.md' | fzf -1);}

# Easy configuration management for my dotfiles.
# ----------------------------------------------
alias bashconfig="$EDITOR   $DOTFILES/shell/bashrc"
alias comenvconfig="$EDITOR $DOTFILES/shell/commonenv.sh"
alias gitconfig="$EDITOR    $DOTFILES/git/gitconfig"
alias tmuxconfig="$EDITOR   $DOTFILES/tmux.conf"
alias vimconfig="$EDITOR    $DOTFILES/vim/vimrc"
alias vimpconfig="$EDITOR   $DOTFILES/vimperator/vimperatorrc"
alias zshconfig="$EDITOR    $DOTFILES/shell/zshrc"
alias rangerconfig="$EDITOR $DOTFILES/ranger"


# Development Environments (Auto-Installed)
# -----------------------------------------
_rbenv_url="https://github.com/sstephenson/rbenv.git"
_rbenv_dir="$HOME/.rbenv"

_pyenv_url="https://github.com/yyuu/pyenv.git"
_pyenv_dir="$HOME/.pyenv"

_pyenv_ve_url="https://github.com/yyuu/pyenv-virtualenv.git"
_pyenv_ve_dir="$_pyenv_dir/plugins/pyenv-virtualenv"

# Clone any environment repo's not yet installed.
[[ -d $_rbenv_dir ]]    || git clone $_rbenv_url $_rbenv_dir
[[ -d $_pyenv_dir ]]    || git clone $_pyenv_url $_pyenv_dir
[[ -d $_pyenv_ve_dir ]] || git clone $_pyenv_ve_url $_pyenv_ve_dir

# Export environment variables.
export PYENV_ROOT="$_pyenv_dir"

addToPATH "$_rbenv_dir/bin" end
addToPATH "$_pyenv_dir/bin" end

# Activate the environments.
eval "$(rbenv init -)"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

# Load my Dyn Developer Environment.
# ----------------------------------
source /Dyn/.dyn_env.sh


# Fuzzy Finder (fzf)
# -----------------
_fzfd=$HOME/.fzf
_fzf_url="https://github.com/junegunn/fzf.git"
[[ -d $_fzfd ]] || { git clone --depth 1 $_fzf_url $_fzfd ; $_fzfd install ;}

# Quick open a Go file.
gogo(){ vim $(locate -A $GOPATH '.go' | fzf -1)  ;}
govim(){ vim $(locate -A $VIMDIR '.vim' | fzf -1)  ;}
gopy(){ vim $(locate -A . '.py' | fzf -1)  ;}
godf(){ vim $(locate -A $DOTFILES | fzf -1)  ;}
godyn(){ vim $(locate -A $DYN_DIR | fzf -1)  ;}
dyn(){ vim $(locate -A $DYN_DIR/jlogemann | fzf -1)  ;}


# Git/VCS Aliases
# ---------------
alias g="git"

alias ga="git add"
alias gA="git add -u"
alias gb="git branch"

alias gc-am="git commit --amend"
alias gc="git commit"
alias gco="git checkout"
alias gcl="git clone"

alias gl="git log"

alias gpl="git pull"
alias gps="git push"

alias grs="git reset HEAD"
alias grb="git rebase -i"

alias gs="echo ''; git status -s"

# Git Flow
alias gf="git flow"
alias gff="git flow feature"
alias gfi="git flow init"
alias gff="git flow feature"
alias gfr="git flow release"
alias gfh="git flow hotfix"
alias gfs="git flow support"
alias gfc="git flow config"

# Helpful sudo aliases.
alias apt-get="sudo apt-get -q"

is_installed "jira" && {
    alias j="jira"
    alias jtodo="jira running"
    alias jissue="jira show"
    alias jopen="jira open"
}

# Golang Aliases
# --------------
is_installed "go" && {
    addToPATH "$GOROOT/bin" end
    addToPATH "$GOPATH/bin" end
}

# Docker Aliases
# --------------
is_installed "docker" && {
    alias dockerScratchUbuntu="docker run --rm -it ubuntu"
    alias dockerScratchDebian="docker run --rm -it debian"
}

# Xorg Display Setup
# ------------------
if [[ ! -z $DISPLAY ]]; then
    setxkbmap \
        -option "ctrl:nocaps" \
        -option "terminate:ctrl_alt_bksp" \
        >/dev/null 2>&1
fi

[[ -d $HOME/.i3/bin ]] && export PATH=$PATH:$HOME/.i3/bin
[[ -f /etc/profile.d/dev-envs.sh ]] && source /etc/profile.d/dev-envs.sh
# vim: ft=sh.bash
