#!/bin/bash
readonly dir="$HOME/.dotfiles"
cd ~/

linkSrc(){
    rm -f $2
    ln -s $1 $2
}

# Links some files into HOME
linkSrc  $dir/Xresources                $HOME/.Xresources
linkSrc  $dir/git/gitconfig             $HOME/.gitconfig
linkSrc  $dir/ranger                    $HOME/.config/ranger
linkSrc  $dir/shell/bash                $HOME/.bash
linkSrc  $dir/shell/bash/bashrc         $HOME/.bashrc
linkSrc  $dir/shell/commonenv.sh        $HOME/.commonenv.sh
linkSrc  $dir/shell/zsh                 $HOME/.zsh
linkSrc  $dir/shell/inputrc             $HOME/.inputrc
linkSrc  $dir/shell/zsh/zshrc           $HOME/.zshrc
linkSrc  $dir/tmux.conf                 $HOME/.tmux.conf
linkSrc  $dir/vim                       $HOME/.vim
linkSrc  $dir/vimperator                $HOME/.vimperator
linkSrc  $dir/vimperator/vimperatorrc   $HOME/.vimperatorrc

# Update XResources
xrdb -merge ~/.Xresources 2>/dev/null
